<?php
/**
 * Created by Rubikin Team.
 * Date: 4/19/14
 * Time: 9:59 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentsGHNBundle;


use Nilead\ShipmentCommonBundle\CarrierAbstract;
use Nilead\ShipmentComponent\Carrier\CarrierInterface;

class Carrier extends CarrierAbstract implements CarrierInterface
{
    public function getCode()
    {
        return 'ghn';
    }

    public function getName()
    {
        return 'GHN Web Service';
    }

    public function getCurrency()
    {
        return 'VND';
    }

    public static function getRatesAvailable()
    {
        return [
            1   =>	'Gói tiết kiệm',
            2	=>  'Gói giao nhanh',
            4	=>  'Gói giao qua ngày',
            12	=>  'Gói siêu tốc liên tỉnh',
            13	=>  'Gói giao nhanh liên tỉnh',
            14	=>  'Gói giao tiết kiệm liên tỉnh',
            17	=>  'Gói siêu tốc (kiện)',
            18	=>  'Gói tiết kiệm (kiện)'
        ];
    }
}
