<?php
/**
 * Created by Rubikin Team.
 * Date: 4/19/14
 * Time: 11:31 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentsGHNBundle\Message;


use Nilead\ShipmentCommonBundle\Message\OrderRequestTrait;
use Nilead\ShipmentsGHNBundle\DistrictCodeMapper;

class CancelOrderRequest extends RequestAbstract
{
    public function setOrderCode($orderCode)
    {
        return $this->setParameter('GHNOrderCode', $orderCode);
    }

    public function getOrderCode()
    {
        return $this->getParameter('GHNOrderCode');
    }

    public function getDefaultParameters()
    {
        return array_merge(parent::getDefaultParameters(),
            [
                'SessionToken' => '',
                'GHNOrderCode' => '',
            ]);
    }

    public function getData()
    {
        $data = array_merge($this->getBaseData(),
            [
                "GHNOrderCode" => $this->getOrderCode(),
                'SessionToken' => $this->getSessionToken()
            ]);

        return $data;
    }

    public function sendData($data)
    {
        $httpResponse = $this->httpClient->post($this->getEndpoint() . '/CancelOrder', array('Content-Type' => 'application/json'), json_encode($data))->send();

        return $this->response = new RatesResponse($this, $httpResponse->json());
    }

} 