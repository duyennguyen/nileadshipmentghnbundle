<?php
/**
 * Created by Rubikin Team.
 * Date: 4/19/14
 * Time: 11:31 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentsGHNBundle\Message;


use Nilead\ShipmentCommonBundle\Message\OrderRequestTrait;
use Nilead\ShipmentsGHNBundle\DistrictCodeMapper;

class OrderRequest extends RequestAbstract
{
    use OrderRequestTrait;

    public function setWeight($weight)
    {
        return $this->setParameter('Weight', $weight);
    }

    public function getWeight()
    {
        return $this->getParameter('Weight');
    }

    public function setFromDistrictCode($fromDistrictCode)
    {
        return $this->setParameter('FromDistrictCode', $fromDistrictCode);
    }

    public function getFromDistrictCode()
    {
        return $this->getParameter('FromDistrictCode');
    }

    public function setToDistrictCode($toDistrictCode)
    {
        return $this->setParameter('ToDistrictCode', $toDistrictCode);
    }

    public function getToDistrictCode()
    {
        return $this->getParameter('ToDistrictCode');
    }

    public function setShippingServiceID($shippingServiceID)
    {
        return $this->setParameter('ShippingServiceID', $shippingServiceID);
    }

    public function getShippingServiceID()
    {
        return $this->getParameter('ShippingServiceID');
    }

    public function getDefaultParameters()
    {
        return array_merge(parent::getDefaultParameters(),
            [
                'SessionToken' => '',
                'Weight' => '',
                'FromDistrictCode' => '',
                'ToDistrictCode' => '',
                'ShippingServiceID' => ''
            ]);
    }

    public function getData()
    {
        $data = array_merge($this->getBaseData(),
            [
                "RecipientCode" => $this->getRecipientId(),
                "RecipientName" => $this->getPackage()->getShippedToAddress()->getLastName() . ' ' . $this->getPackage()->getShippedToAddress()->getFirstName(),
                "RecipientPhone" => $this->getPackage()->getShippedToAddress()->getPhone(),
                "DeliveryAddress" => $this->getPackage()->getShippedToAddress()->getAddress1() . ' ' . $this->getPackage()->getShippedToAddress()->getAddress2(),
                "DeliveryDistrictCode" => DistrictCodeMapper::fromGeoCode($this->getPackage()->getShippedToAddress()),
                "CODAmount" => $this->getCOD(),
                "Note" => $this->getNote(),
                "VendorCode" => $this->getShopId(),
                "ShopOrderCode" => $this->getOrderId(),
                "SenderName" => $this->getPackage()->getShippedToAddress()->getLastName() . ' ' . $this->getPackage()->getShippedToAddress()->getFirstName(),
                "SenderPhone" => $this->getPackage()->getShippedToAddress()->getPhone(),
                "SenderAddress" => $this->getPackage()->getShippedFromAddress()->getAddress1() . ' ' . $this->getPackage()->getShippedFromAddress()->getAddress2(),
                "PickAddress" => $this->getPackage()->getShippedFromAddress()->getAddress1() . ' ' . $this->getPackage()->getShippedFromAddress()->getAddress2(),
                "PickDistrictCode" => DistrictCodeMapper::fromGeoCode($this->getPackage()->getShippedFromAddress()),
                "ServiceToUse" => $this->getMethodCode(),
                'SessionToken' => $this->getSessionToken()
            ]);

        return $data;
    }

    public function sendData($data)
    {
        $httpResponse = $this->httpClient->post($this->getEndpoint() . '/CreateShippingOrder', array('Content-Type' => 'application/json'), json_encode($data))->send();file_put_contents(__DIR__ . '/test2.txt', $httpResponse->getBody());
        return $this->response = new OrderResponse($this, $httpResponse->json());
    }

} 