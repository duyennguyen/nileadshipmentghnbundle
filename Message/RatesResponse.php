<?php
/**
 * Created by Rubikin Team.
 * Date: 4/20/14
 * Time: 1:10 PM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentsGHNBundle\Message;

use Nilead\ShipmentCommonComponent\Message\RatesResponseInterface;
use Nilead\ShipmentCommonComponent\Message\RatesRequestInterface;
use Nilead\PricingBundle\Model\DecimalPrice;

class RatesResponse extends ResponseAbstract implements RatesResponseInterface
{
    protected $data;

    /**
     * @var RatesRequestInterface
     */
    protected $request;

    public function __construct(RatesRequestInterface $request, $data)
    {
        $this->request = $request;
        $this->data = $data;
    }

    /**
     * (@inheritdoc)
     */
    public function isSuccessful()
    {
        return $this->data['ResponseException'] == null;
    }

    /**
     * (@inheritdoc)
     */
    public function getRates()
    {
        $rates = [];

        $methods = $this->request->getMethods();
        foreach ($this->data['Items'] as $item) {
            $methodCode = $item['ShippingServiceID'];
            if (isset($methods[$methodCode])) {
                $rates[$methodCode] = [
                    'name' => $methods[$methodCode]->getName(),
                    'id' => $methods[$methodCode]->getId(),
                    'total_price' => new DecimalPrice(
                            $item['ShippingServiceID'], $item['ShippingServiceID'], $item['ShippingServiceID'], $item['Fee']
                        ),
                    'max_delivery_date' => ''
                ];
            }
        }

        return $rates;
    }
}
