<?php
/**
 * Created by Rubikin Team.
 * Date: 4/20/14
 * Time: 2:00 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentsGHNBundle\Tests\Message;

use Nilead\ShipmentBundle\Tests\TestCase;
use Nilead\ShipmentsGHNBundle\Message\OrderInfoResponse;
use Mockery as m;

class OrderInfoResponseTest extends TestCase
{
    protected $request;

    public function testSuccess()
    {
        $httpResponse = $this->getMockHttpResponse('OrderInfoSuccess.txt');

        $request = $this->getMockRequest();

        $response = new OrderInfoResponse($request, $httpResponse->json());

        $this->assertTrue($response->isSuccessful());

        $this->assertEquals('7184934551', $response->getOrderCode());
    }

    public function testFailure()
    {
        $httpResponse = $this->getMockHttpResponse('OrderInfoFailure.txt');

        $request = $this->getMockRequest();

        $response = new OrderInfoResponse($request, $httpResponse->json());

        $this->assertFalse($response->isSuccessful());
    }
} 