<?php
/**
 * Created by Rubikin Team.
 * Date: 4/20/14
 * Time: 2:00 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentsGHNBundle\Tests\Message;

use Nilead\ShipmentBundle\Tests\TestCase;
use Nilead\ShipmentCommonBundle\Tests\TraitPackageTest;
use Nilead\ShipmentsGHNBundle\Message\RatesRequest;

class RatesRequestTest extends TestCase
{
    use TraitPackageTest;

    protected $request;

    public function setUp()
    {
        $this->request = new RatesRequest();
        $this->request->setServices($this->getHttpClient(), $this->getHttpRequest());
    }

    public function testGetData()
    {

        $this->request->setClientID(2619)
            ->setSessionToken('5512b651-d6e8-4fc4-b935-268f4d9e0f43')
            ->setPassword('JpTwK4tNkm1VENv8V')
            ->setWeight(100)
            ->setFromDistrictCode('1A01')
            ->setToDistrictCode('1A06')
            ->setShippingServiceID(1)
            ->setMethods([1 => $this->generateShippingMethod('1')]);

        $items = [[
            "Weight" => 100,
            "FromDistrictCode" => "1A01",
            "ToDistrictCode" => "1A06",
            "ShippingServiceID" => 1
        ]];

        $this->assertEquals([
                "ClientID" => 2619,
                "Password" => "JpTwK4tNkm1VENv8V",
                "VersionNo" => NULL,
                "SessionToken" => "5512b651-d6e8-4fc4-b935-268f4d9e0f43",
                "Items" => $items
            ]
            , $this->request->getData());
    }
} 