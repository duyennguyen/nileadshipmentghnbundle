<?php
/**
 * Created by Rubikin Team.
 * Date: 4/20/14
 * Time: 2:00 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentsGHNBundle\Tests\Message;

use Nilead\ShipmentBundle\Tests\TestCase;
use Nilead\ShipmentsGHNBundle\Message\RatesResponse;
use Mockery as m;

class RatesResponseTest extends TestCase
{
    protected $request;

    public function testSuccess()
    {
        $httpResponse = $this->getMockHttpResponse('RatesSuccess.txt');

        $request = $this->getMockRequest();

        $request->shouldReceive('getMethods')->once()->andReturn([
            1 => m::mock('\Nilead\ShipmentComponent\Model\ShippingMethodInterface')
                    ->shouldReceive('getName')->andReturn('name')->getMock()
                    ->shouldReceive('getId')->andReturn('id')->getMock()

        ]);

        $response = new RatesResponse($request, $httpResponse->json());

        $rates = $response->getResult();

        $this->assertTrue($response->isSuccessful());
        $this->assertEquals(10000, $rates[1]['total_price']->getTotal());
    }

    public function testFailure()
    {
        $httpResponse = $this->getMockHttpResponse('RatesFailure.txt');

        $request = $this->getMockRequest();

        $response = new RatesResponse($request, $httpResponse->json());

        $this->assertFalse($response->isSuccessful());
    }
} 