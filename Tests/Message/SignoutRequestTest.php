<?php
/**
 * Created by Rubikin Team.
 * Date: 4/20/14
 * Time: 2:00 AM
 * Question? Come to our website at http://rubikin.com
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Nilead\ShipmentsGHNBundle\Tests\Message;

use Nilead\ShipmentBundle\Tests\TestCase;
use Nilead\ShipmentsGHNBundle\Message\SignoutRequest;

class SignoutRequestTest extends TestCase
{
    protected $request;

    public function setUp()
    {
        $this->request = new SignoutRequest();
        $this->request->setServices($this->getHttpClient(), $this->getHttpRequest());
    }

    public function testGetData()
    {
        $this->request->setSessionToken('d5e93d20-8ab7-4d3a-9cee-7185603f3fbc');

        $this->assertEquals([
            "SessionToken" => "d5e93d20-8ab7-4d3a-9cee-7185603f3fbc"
            ],
        $this->request->getData());
    }
} 